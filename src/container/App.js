import './App.css';
import React, { Component } from 'react'
import Wave from '../Component/Wave/Wave';
import Todo from './Todo';


class App extends Component{
  render() {
    return (
      <div>
        <Todo/>
        <Wave />
      </div>
      
    )
  }
}

export default App;
