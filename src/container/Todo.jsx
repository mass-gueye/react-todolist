import React, { useState } from "react";
import Todos from "../Component/Todos/Todos";
import "./todo.styles.css";

const current = new Date();
const date = `${current.getDate()}/${current.getMonth()+1}/${current.getFullYear()}`;


const Todo = () => {
  const [inputText, setinputText] = useState("");
  const [todos, setTodos] = useState([
    {
      text: "Welcome to your todolist !",
      completed: false,
      id: Math.random() * 1000,
    },
    {
      text: "Hit the + button to add a task",
      completed: false,
      id: Math.random() * 1000,
    },
    {
      text: "<-- Hit this to delete an item",
      completed: false,
      id: Math.random() * 1000,
    },
  ]);

  const handleChange = (e) => {
    e.preventDefault();
    setinputText(e.target.value);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    setTodos([
      ...todos,
      { text: inputText, completed: false, id: Math.random() * 1000 },
    ]);
    setinputText("");

  };
  return (
    <div className="container">
      <h1>Date: {date}</h1>
      <Todos todos={todos} setTodos={setTodos}/>

      <form className="item todos" onSubmit={handleSubmit}>
        <input type="text" placeholder="New Task" value={inputText} onChange={handleChange} required />
        <button type="submit">+</button>
      </form>
    </div>
  );
};

export default Todo;
