import React from 'react'
import './wave.styles.css'
import wave from '../../images/wave.svg'
const Wave = () => {
    return (
        <>
            <img src={wave} alt="" className="svg" />
        </>
    )
}

export default Wave
