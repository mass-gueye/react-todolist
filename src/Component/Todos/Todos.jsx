import './todos.styles.css'


const Todos = ({ todos,setTodos }) => {
  const handleDelete = (e) => {
    e.target.form.remove();
  }
 
  return (
    <>
      {todos.map((todo, idx) => (
        <form action="" className="todos" key={idx}>
        <div className="item">
            <input type="checkbox" name="" id="" onClick={handleDelete}/>
            <p>{todo.text}</p>
        </div>
        <hr />
      </form>
      ))}
    </>
  );
};

export default Todos;
